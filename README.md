# Chiasenhac Get Link

Thực ra thì các bạn cũng chỉ đăng ký nhanh một tài khoản là tải được vê thôi nhưng mà mình lười lên làm 1 cái gì đó tải all in one về cho nhanh
### Bước 1: Lấy Link bài hát về
dạng link nghe nhạc sẽ là: https://vn.chiasenhac.vn/mp3/vietnam/v-pop/co-em-doi-bong-vui~chillies~tsvwr7zsq9at14.html
### Bước 2: view code
tìm: music_id lấy giá trị của nó
```
                    <div class="form-group emoji-picker-container">
                        <textarea class="form-control comment" name="comment" rows="3" placeholder="Viết bình luận của bạn tại đây." data-emojiable="true"></textarea>
                        <input type="hidden" class="music_id" name="music_id" value="2060821">
                        <button onclick="postComment(0)"  class="btn my-2 my-sm-0 waves-effect waves-light btn-upload btn_cloud_up" style="float: right; min-width: 75px;">Đăng Bình Luận</button>
                    </div>
                </form>
```
### Bước 3: đưa value của music_id vào api sau:
```http://old.chiasenhac.vn/api/listen.php?code=csn22052018&return=json&m=2060821```
thay 2060821 thành Id của bạn: bạn sẽ được thông tin như dưới đây

```
    "file_url": "http://data25.chiasenhac.com/downloads/2061/1/2060821-946b2223/128/Anh%20Thanh%20Nien%20-%20HuyR.mp3",
    "file_32_url": "http://data25.chiasenhac.com/downloads/2061/1/2060821-946b2223/32/Anh%20Thanh%20Nien%20-%20HuyR.m4a",
    "file_320_url": "http://data25.chiasenhac.com/downloads/2061/1/2060821-946b2223/320/Anh%20Thanh%20Nien%20-%20HuyR.mp3",
    "file_m4a_url": "http://data25.chiasenhac.com/downloads/2061/1/2060821-946b2223/m4a/Anh%20Thanh%20Nien%20-%20HuyR.m4a",
    "file_lossless_url": "http://data25.chiasenhac.com/downloads/2061/1/2060821-946b2223/flac/Anh%20Thanh%20Nien%20-%20HuyR.flac",
    "full_url": "http://mp3.chiasenhac.vn/mp3/vietnam/v-pop/anh-thanh-nien~huyr~tsvwbsw6q9q49k.html",
```
chúc bạn may mắn!
